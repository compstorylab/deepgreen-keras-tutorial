import itertools
from typing import Hashable, List, Optional

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.metrics import classification_report, confusion_matrix
from tensorflow.keras.utils import to_categorical
from tensorflow.python.client import device_lib


def main(batch_size: Optional[int] = None, epochs: int = 1, val_frac: float = 0.2):
    """
    Args:
        batch_size: Number of samples used to construct each gradient update.
        epochs: Number of times the dataset will be exposed to the model.
        val_frac: Fraction of training data to use for validation.
    """
    detected_gpus = len(available_gpus())
    if batch_size is None:
        batch_size = 512 * detected_gpus

    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

    # Rescale image values: [0, 255] -> [0, 1]
    x_train = (x_train.astype("float32") / 255.0)[..., np.newaxis]
    x_test = (x_test.astype("float32") / 255.0)[..., np.newaxis]

    # Split off a validation set
    inds = np.random.permutation(len(x_train))
    split_ind = int(len(x_train) * (1 - val_frac))
    train_inds, val_inds = inds[:split_ind], inds[split_ind:]
    x_val, y_val = x_train[val_inds], y_train[val_inds]
    x_train, y_train = x_train[train_inds], y_train[train_inds]

    print(f"Train shape:      {x_train.shape}")
    print(f"Validation shape: {x_val.shape}")
    print(f"Test shape:       {x_test.shape}")

    # Convert integer class labels to one-hot encoded class vectors
    y_train, y_val, y_test = (
        to_categorical(y_train),
        to_categorical(y_val),
        to_categorical(y_test),
    )

    # Mirrored strategy allows for model parallelism
    # See https://keras.io/guides/distributed_training/ for more details
    strategy = tf.distribute.MirroredStrategy()

    with strategy.scope():
        model = build_neural_network()
        model.compile(
            optimizer="adam",
            loss="categorical_crossentropy",
            metrics=["accuracy"],
        )
    model.summary()

    print(
        f"\n\n{detected_gpus} GPUs detected.\n"
        f"{strategy.num_replicas_in_sync} model replicas in use.\n\n"
    )

    checkpoint = tf.keras.callbacks.ModelCheckpoint(
        "../output/weights.h5",
        monitor="val_accuracy",
        save_weights_only=True,
        save_best_only=True,
    )

    model.fit(
        x_train,
        y_train,
        batch_size=batch_size,
        epochs=epochs,
        verbose=2,
        validation_data=(x_val, y_val),
        callbacks=[checkpoint],
    )

    model.load_weights("../output/weights.h5")
    score = model.evaluate(x_test, y_test, verbose=1, batch_size=batch_size)
    print(f"Test score:    {score[0]: .4f}")
    print(f"Test accuracy: {score[1] * 100.:.2f}")

    preds = model.predict(x_test, batch_size=batch_size)

    c = confusion_matrix(np.argmax(y_test, axis=-1), np.argmax(preds, axis=-1))
    plot_confusion_matrix(
        c,
        list(range(10)),
        normalize=False,
        output_path="../output",
    )
    plot_confusion_matrix(
        c,
        list(range(10)),
        normalize=True,
        output_path="../output",
    )

    print(
        classification_report(
            np.argmax(y_test, axis=-1),
            np.argmax(preds, axis=-1),
            target_names=[str(x) for x in range(10)],
        )
    )


def build_neural_network() -> tf.keras.models.Sequential:
    """
    Constructs a simple convolutional neural network with 2 convolution blocks
    followed by a single dense block. Drop block and dropout are used for regularization.
    """
    model = tf.keras.models.Sequential()
    model.add(
        tf.keras.layers.SpatialDropout2D(
            input_shape=(28, 28, 1),
            rate=0.2,
            name="Input-Dropout",
        )
    )
    model.add(
        tf.keras.layers.Conv2D(
            filters=64,
            kernel_size=3,
            activation="relu",
            padding="same",
            name="Conv-1",
        )
    )
    model.add(tf.keras.layers.MaxPool2D(pool_size=2, name="Pool-1"))
    model.add(tf.keras.layers.SpatialDropout2D(rate=0.2, name="Dropout-1"))
    model.add(
        tf.keras.layers.Conv2D(
            filters=32,
            kernel_size=3,
            activation="relu",
            padding="same",
            name="Conv-2",
        )
    )
    model.add(tf.keras.layers.MaxPool2D(pool_size=2, name="Pool-2"))
    model.add(tf.keras.layers.SpatialDropout2D(rate=0.2, name="Dropout-2"))
    model.add(tf.keras.layers.Flatten(name="Flatten"))
    model.add(tf.keras.layers.Dense(units=256, activation="relu", name="Dense"))
    model.add(tf.keras.layers.Dropout(rate=0.2, name="Dense-Dropout"))
    model.add(tf.keras.layers.Dense(units=10, activation="softmax", name="Softmax"))
    return model


def available_gpus() -> List[str]:
    """
    Returns:
        List of names of available GPUs.
    """
    return [x.name for x in device_lib.list_local_devices() if x.device_type == "GPU"]


def plot_confusion_matrix(
    cm: np.ndarray,
    classes: List[Hashable],
    normalize: bool = False,
    title: str = "Confusion matrix",
    cmap: str = "Blues",
    output_path: str = ".",
):
    """
    Logs and plots a confusion matrix, e.g. text and image output.

    Adapted from:
        http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    Args:
        cm: Confusion matrix to be plotted.
        classes: Labels for the classes.
        normalize: Whether to normalize the confusion matrix before plotting, or not.
        title: A title to be added to the plot.
        cmap: Name of the matplotlib colormap to be used in plotting.
        output_path: Where the outputs should be placed.
    """
    if normalize:
        cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
        tag = "_norm"
        print("Normalized confusion matrix:")
    else:
        tag = ""
        print("Confusion matrix:")
    print(cm)

    plt.imshow(cm, interpolation="nearest", cmap=plt.get_cmap(cmap))
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = ".2f" if normalize else "d"
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black",
        )
    plt.ylabel("True label")
    plt.xlabel("Predicted label")
    plt.tight_layout()
    plt.savefig(f"{output_path}/confusion{tag}.png")
    plt.close()


if __name__ == "__main__":
    main(epochs=200)
