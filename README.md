# Tutorial: Deep Learning on the VACC


## Table of Contents
- [Accessing BlueMoon and DeepGreen](#accessing-bluemoon-and-deepgreen)
- [Clone This Repository](#clone-this-repository)
- [Getting Started](#getting-started)
- [Setting Up Your Python Environment](#setting-up-your-python-environment)
  - [Install Anaconda](#install-anaconda)
  - [Installing Deep Learning Packages](#installing-deep-learning-packages)
- [Deep Learning On DeepGreen](#deep-learning-on-deepgreen)
- [VACC File Systems](#vacc-file-systems)
- [Setting Up A SSH Key](#setting-up-a-ssh-key)
- [Additional Resources](#additional-resources)


## Accessing BlueMoon and DeepGreen
All VACC computing resources, including DeepGreen, are accessed through a set of shared user nodes.
The most common way to do this is via SSH, using your NetID and associated password:
```bash
ssh {your NetID}@vacc-user1.uvm.edu
{your NetID}@vacc-user1.uvm.edu's password: {your NetID password}
```

For example:
```bash
ssh cvanoort@vacc-user1.uvm.edu
cvanoort@vacc-user1.uvm.edu's password: {cvanoort's password}
```

There are two user nodes, `vacc-user1` and `vacc-user2`.
If you're ever having an issue with connectivity or responsiveness, then see if the other user node works better for you. 
Once you've successfully ssh-ed onto a user node, you need to set up your development environment before you can start submitting jobs to DeepGreen.

## Clone This Repository
Before you get too far, you'll want to clone this repository onto the VACC so that you'll have access to the source code.
```bash
cd ~/scratch
git clone https://gitlab.com/compstorylab/deepgreen-keras-tutorial.git
```

## Getting Started
The easiest way to get started is to run
```
bash ~/scratch/deepgreen-keras-tutorial/setup_conda.sh
```
This will create a fresh Anaconda install, create a new conda environment
titled `tf2`, and install the packages required to run the example
code contained in `src/`.

Following this, you will need to run `source ~/.bashrc` or create a new
ssh session in order to use your new python setup.

If you want more control over your setup, or you're just curious, then read on.


## Setting Up Your Python Environment
### Install Anaconda
Anaconda gives you more control over the python version and package
versions that you'll be using.
It also greatly simplifies the setup of common deep learning packages,
like Tensorflow, Keras, and Pytorch.

Setting up Anaconda:
```bash
cd ~/scratch
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -bfu -p ~/scratch/miniconda3
```

You can copy and paste the lines above into your terminal or just run the following:
```bash
bash ~/scratch/deepgreen-keras-tutorial/setup_conda.sh
```

### Installing Deep Learning Packages
Once you have a python environment, you'll need to install some packages
in order to start doing some deep learning.

If you did not run `setup_conda.sh` then you'll need to install some
dependencies here.
The easiest way to do this is `conda env create -f deepgreen-keras-tutorial/tf2.yml`.
This creates a new Anaconda environment, similar to a Python virtual environment (venv),
and installs the requirements listed in `tf2.yml`.

You can install additional packages using `conda install {package names}` or
```
conda activate tf2
pip install {package names}
```
Note: Older Anaconda installations may need to use `source activate {env name}`.

## Deep Learning On DeepGreen
In the `src/` folder you will find two files, `submit.sh` and `train.py`.
`train.py` is an example script that shows how to used Keras to
parallelize the training of a simple image classification network across
multiple GPUs.
By default, the script will attempt to use all available GPUs,
and will indicate how many GPUs were detected.
This implementation uses data parallelism, which involves:
 - Each GPU receives an identical copy of the network being trained
 - Batch is split into chunks, where each GPU processes one chunk
 - The gradients from each chunk are combined on the CPU
 - The model held by each GPU is updated synchronously
 - Repeat.
 
There are other ways the deep learning can be parallelized, see
[this paper](https://arxiv.org/abs/1802.09941) for more details.

DeepGreen is a shared computing resource, meaning that many researchers
and research groups can use one or more of its nodes simultaneously.
The user nodes that you SSH into are not meant to perform serious number
crunching, so we use `submit.sh` to submit our job to the cluster scheduler.

Jobs intended for any VACC hardware are managed by the [slurm scheduler](https://slurm.schedmd.com/).
Slurm submission scripts specify the amount of resources that a job
should be allocated, including node count, CPU count, GPU count, memory,
and wall clock time.
Following these specifications, the body of the job is composed of
arbitrary bash commands, including activating conda environments and
executing python scripts.

For example, `submit.sh` requests a single node, 2 CPU cores, 2 GPUs,
16 GB of memory, and 10 min of runtime.
In the body of `submit.sh` we simply activate the conda environment that
has our dependencies installed, and call `train.py`.

We can submit our job to the scheduler using `sbatch submit.sh`.
We can check the status of the job queue using `squeue`, and we can
filter the results to show just our jobs using `squeue -u {username}`.

[This cheat sheet](https://www.chpc.utah.edu/presentations/SlurmCheatsheet.pdf)
provides an overview of the important slurm commands.

Only a subset of the available job configuration options are used in `submit.sh`.
In general, almost any option for the `sbatch` command can be included in
the header of your job script.
See the [`sbatch` man page](https://slurm.schedmd.com/sbatch.html) for
details about additional options.

In addition to batch jobs, you can also submit interactive jobs to the
scheduler using the `srun` command.
The DeepGreen docs have an [example usage of `srun`](https://wiki.uvm.edu/w/DeepGreenDocs#Submitting_an_interactive_job).

If you are familiar with PBS/Torque, an alternative HPC job scheduling software, it should
be easy for you to get started with slurm.
There is a correspondence between the commands that are used to interact
with both of these schedulers, see [this cheat sheet](https://slurm.schedmd.com/rosetta.pdf)
for more details.

The main way that you submit jobs to the various VACC systems is by indicating a desired partition in the header of your job script.
Available partitions are listed in the VACC knowledge base:
 - [Bluemoon Partitions](https://www.uvm.edu/vacc/kb/knowledge-base/write-submit-job-bluemoon/#partition): `bluemoon`, `short`, `week`, `bigmem`, `bigmemwk`, and `ib`
 - [DeepGreen Partitions](https://www.uvm.edu/vacc/kb/knowledge-base/write-submit-job-deepgreen/#partition): `dggpu`
 - [Blackdiamond Partitions](https://www.uvm.edu/vacc/kb/knowledge-base/blackdiamond-writing-submitting-a-job/#partition): `bdgpu`


## VACC File Systems
There are three file systems on the VACC.
- `gpfs1`:
  - Spinning disk storage
  - Data is backed up
  - Has file count and file size quotas
  - File count and file size quotas are more strict
  - Available to all nodes
- `gpfs2`:
  - Spinning disk storage
  - Data is not backed up
  - Has file count and file size quotas
  - File count and file size quotas are less strict
  - Available to all nodes
- `gpfs3`:
  - NVMe storage (much faster than `gpfs1` or `gpfs2`)
  - Data backed up (2x redundancy)
  - Does not have file count or file size quotas
  - Only available to DeepGreen (`dggpu`) and BlackDiamond (`bdgpu`) nodes

Your home directory is stored in `gpfs1`, your scratch directory is on
`gpfs2`, and when using DeepGreen or BlackDiamond you can store files at
`/gpfs3/scratch/{username}`.

It is recommended that you store most files on `gpfs2`, since it has less strict quotas.
You may want to use `gpfs3` for intermediate results that are created by your scripts, for performance reasons.
However, you should be sure to move those results to `gpfs1` or `gpfs2` when your job is complete.
Since `gpfs3` does not currently have quotas, it is up to each user to utilize the space reasonably.


## Setting Up A SSH Key
You might not want to type your username and password everytime you want to SSH onto the VACC.
Fortunately, there's a way to fix that!

If you do not already have a ssh key for your machine, you'll need to generate one:
```
ssh-keygen -t rsa
```

This will make a ssh config folder, a public key, and a private key:
```
~/.ssh/
    ~/.ssh/id_rsa
    ~/.ssh/id_rsa.pub
```

Next, you'll need to make and populate a ssh config file:
```
mkdir ~/.ssh
vim ~/.ssh/config
```

You'll want to populate the ssh config file with aliases for the BlueMoon user nodes:
```
Host vacc
    HostName vacc-user1.uvm.edu
    User {your NetID}

Host vacc2
    HostName vacc-user2.uvm.edu
    User {your NetID}
```

Finally, you need to send your public key to the VACC:
```
ssh-copy-id -i ~/.ssh/id_rsa.pub {your NetID}@vacc-user1.uvm.edu
ssh-copy-id -i ~/.ssh/id_rsa.pub {your NetID}@vacc-user2.uvm.edu
```
If everything is set up correctly, now you should be able to use `ssh vacc` and `ssh vacc2` to access the VACC without entering your password.

## Additional Resources:
 - [VACC Documentation](https://wiki.uvm.edu/index.php?title=VACCUserDocumentation)
 - [DeepGreen Documentation](https://wiki.uvm.edu/w/DeepGreenDocs)
 - [Tensorflow on the VACC](https://wiki.uvm.edu/w/VACCTensorFlow)
